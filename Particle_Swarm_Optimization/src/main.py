from PSO import Swarm
from parser import parse_json_config
from exceptions import NoConfig, SubprocessError, NoCommand, NoParameterBounds
from stats import save_stats_to_csv
from stats import visualize_iter_value_graph
from multiprocess import Pool
from subproc import run_subprocess


def high_level_PSO_call(cfg_path=None, exit_criteria_max_iter=False, csv_name: str = 'stats_convergence'):
    try:
        params, cfg_path = parse_json_config(cfg_path)

        def func(args):
            return run_subprocess(params["command_to_run"], args)

        with Pool(params["pool_size"]) as pool:
            swarm = Swarm(parameter_bounds=params["parameter_bounds"],
                          function=func,
                          num_particles=params["num_particles"],
                          w=params["w"],
                          c_1=params["c_1"],
                          c_2=params["c_2"],
                          pool_size=params["pool_size"],
                          pool=pool,
                          exit_criteria_max_iter=exit_criteria_max_iter)
            minimum, f_g_bests = swarm.optimize()
            visualize_iter_value_graph(f_g_bests, '_'.join(cfg_path.split('_')[:-2]))
            save_stats_to_csv(cfg_path, params["pool_size"],
                swarm.iter_number, swarm.f_g_best, swarm.g_best, swarm.time,
                params["num_particles"], params["w"], params["c_1"], params["c_2"],
                csv_name)
            print(minimum)
    except NoConfig as e:
        print(e.msg)
    except NoCommand as e:
        print(e.msg)
    except NoParameterBounds as e:
        print(e.msg)
    except SubprocessError as e:
        print(e.msg)


if __name__=='__main__':
    high_level_PSO_call()
