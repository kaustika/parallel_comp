import json
from PSO import Bounds
import argparse
from exceptions import NoConfig, NoCommand, NoParameterBounds

DEFAULT_NUM_PARTICLES = 200
DEFAULT_W = 0.3
DEFAULF_C_1 = 2
DEFAULF_C_2 = 2
DEFAULT_POOL_SIZE = 10


def set_up_parser():
    parser = argparse.ArgumentParser(description='Particle Swarm Algorithm',
                                     add_help=False)
    parser.add_argument("-c", "--config", action="store", dest="cfg_path",
                        help="path to the configuration file")
    parser.add_argument("-h", "--help", action="help",
                        help="show this help message")
    args = parser.parse_args()
    return args


def write_json_config(parameter_bounds: list[Bounds],
                      command_to_run: str,
                      config_name: str,
                      num_particles: int = DEFAULT_NUM_PARTICLES,
                      w: float = DEFAULT_W,
                      c_1: int = DEFAULF_C_1,
                      c_2: int = DEFAULF_C_2,
                      pool_size: int = DEFAULT_POOL_SIZE
                      ):
    bounds_serialized = Bounds.schema().dump(parameter_bounds, many=True)
    params = {"parameter_bounds": bounds_serialized,
              "command_to_run": command_to_run,
              "num_particles": num_particles,
              "w": w, "c_1": c_1, "c_2": c_2,
              "pool_size": pool_size
              }
    with open(config_name + ".json", "w") as cfg:
        json.dump(params, cfg)


def parse_json_config(cfg_path=None):
    args = set_up_parser()
    if not args.cfg_path and not cfg_path:
        raise NoConfig
    else:
        cfg_path = args.cfg_path if args.cfg_path else cfg_path
        with open(cfg_path) as cfg:
            params = json.load(cfg)

        # These arguments are NOT optional
        if "command_to_run" not in params.keys():
            raise NoCommand
        if "parameter_bounds" not in params.keys():
            raise NoParameterBounds
        else:
            serialized = json.dumps(params["parameter_bounds"])
            params["parameter_bounds"] = Bounds.schema().loads(serialized, many=True)

        # These arguments are optional
        if "num_particles" not in params.keys():
            params["num_particles"] = DEFAULT_NUM_PARTICLES
        if "w" not in params.keys():
            params["w"] = DEFAULT_W
        if "c_1" not in params.keys():
            params["c_1"] = DEFAULF_C_1
        if "c_2" not in params.keys():
            params["c_2"] = DEFAULF_C_2

        return params, cfg_path


