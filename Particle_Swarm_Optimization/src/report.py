import matplotlib.pyplot as plt
from main import high_level_PSO_call
from parser import write_json_config
from PSO import Bounds
import pandas as pd


def generate_config_rosenbrock(pool_size=1):
    parameter_bounds = [Bounds(-20, 20), Bounds(-20, 20)]
    file_name = "rosenbrock_cfg_threads" + str(pool_size)
    command_to_run = "python D:/current_work/POLY/PAR_COMP/parallel_comp/Particle_Swarm_Optimization/src/rosenbrock.py"
    write_json_config(
        parameter_bounds=parameter_bounds,
        command_to_run=command_to_run,
        config_name=file_name,
        pool_size=pool_size)
    return file_name


def generate_config_rastrigin(dim: int = 3, pool_size=1):
    parameter_bounds = [Bounds(-5.12, 5.12) for _ in range(dim)]
    file_name = "rastrigin_" + str(dim) + "D_cfg_threads" + str(pool_size)
    command_to_run = "python D:/current_work/POLY/PAR_COMP/parallel_comp/Particle_Swarm_Optimization/src/rastrigin.py"
    write_json_config(
        parameter_bounds=parameter_bounds,
        command_to_run=command_to_run,
        config_name=file_name,
        pool_size=pool_size)
    return file_name


def generate_config_ackley(pool_size=1):
    parameter_bounds = [Bounds(-5, 5) for _ in range(2)]
    file_name = "ackley_cfg_threads" + str(pool_size)
    command_to_run = "python D:/current_work/POLY/PAR_COMP/parallel_comp/Particle_Swarm_Optimization/src/ackley.py"
    write_json_config(
        parameter_bounds=parameter_bounds,
        command_to_run=command_to_run,
        config_name=file_name,
        pool_size=pool_size)
    return file_name


def plot_speedup(file_path):
    df = pd.read_csv(file_path,
        names=["cfg_name", "pool_size", "iters", "f_g_best", "g_best",
               "time", "num_particles", "w", "c_1", "c_2"])
    func_types = set('_'.join(l[:-2]) for l in df['cfg_name'].str.split('_').to_list())
    for func_type in func_types:
        subset = df[df['cfg_name'].str.startswith(func_type)]
        num_threads = len(subset)
        speedup = [float(subset.iloc[0]['time']) / float(subset.iloc[i]['time']) for i in range(num_threads)]
        plt.plot(range(1, num_threads+1), speedup, label=f'{func_type.capitalize()} Function')
        plt.title('Speedup vs Number of Threads', fontsize=16)
        plt.xlabel('Number of Threads', fontsize=12)
        plt.ylabel('Speedup', fontsize=12)
        plt.legend()
        # plt.show()

    plt.plot(range(1, num_threads+1), range(1, num_threads+1))
    plt.savefig('speedup.png')
    plt.clf()


def plot_efficiency(file_path):
    df = pd.read_csv(
        file_path,
        names=["cfg_name", "pool_size", "iters", "f_g_best", "g_best",
               "time", "num_particles", "w", "c_1", "c_2"])

    func_types = set('_'.join(l[:-2]) for l in df['cfg_name'].str.split('_').to_list())
    for func_type in func_types:
        subset = df[df['cfg_name'].str.startswith(func_type)]
        num_threads = len(subset)
        efficiency = [(float(subset.iloc[0]['time']) / float(subset.iloc[i]['time']))/(i+1) for i in range(num_threads)]
        plt.plot(range(1, num_threads+1), efficiency, label=f'{func_type.capitalize()} Function')
        plt.title('Efficiency vs Number of Threads', fontsize=16)
        plt.xlabel('Number of Threads', fontsize=12)
        plt.ylabel('Efficiency', fontsize=12)
        plt.legend()
        # plt.show()
    plt.plot(range(1, num_threads + 1), [1 for _ in range(num_threads)])
    plt.savefig('efficiency.png')
    plt.clf()


def run_for_diff_num_threads(n=10):
    cfgs = []
    # create configs for different functions and number of threads
    for num_threads in range(1, n + 1):
        cfgs.append(generate_config_rosenbrock(pool_size=num_threads))
        cfgs.append(generate_config_rastrigin(dim=5, pool_size=num_threads))
        cfgs.append(generate_config_rastrigin(dim=10, pool_size=num_threads))
        cfgs.append(generate_config_rastrigin(dim=15, pool_size=num_threads))
        cfgs.append(generate_config_rastrigin(dim=20, pool_size=num_threads))
        cfgs.append(generate_config_ackley(pool_size=num_threads))
    for cfg in cfgs:
        print(cfg)
        high_level_PSO_call(cfg + ".json", exit_criteria_max_iter=True, csv_name='stats_high_dim')


if __name__ == "__main__":
    run_for_diff_num_threads()
    plot_speedup("stats_high_dim.csv")
    plot_efficiency("stats_high_dim.csv")
