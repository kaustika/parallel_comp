class NoConfig(Exception):
    """
    Exception to raise when path to the config file wasn't passed.
    """

    def __init__(self):
        self.msg = "No config-file path!" \
                   "You have to pass path to the configuration file via --config key!"


class NoCommand(Exception):
    """
    Exception to raise when command to run (program calculating the function value
    with params given by the optimizer at each iteration) is not present in the config-file.
    """

    def __init__(self):
        self.msg = "command to run (program calculating the function value" \
                   " with params given by the optimizer at each iteration) is" \
                   " not present in the config-file!"


class NoParameterBounds(Exception):
    """
    Exception to raise when parameter bounds for function to optimize
    are not present in the config-file.
    """

    def __init__(self):
        self.msg = "Parameter bounds for function to optimize" \
                   "are not present in the config-file!"


class SubprocessError(Exception):
    """
    Exception to raise when subprocess returned non-empty stderr.
    """

    def __init__(self):
        self.msg = "Subprocess error! See stderr: "
