import subprocess
from exceptions import SubprocessError


def run_subprocess(command_to_run: str, args_as_py_list: [list]) -> float:
    # Как программа принимает аргументы? Нужно как-то стандартизировать
    # пусть это будет просто n позиционных аргументов по числу размерностей - значения параметров,
    # при которых хотим посчитать значение ф-ии.
    # В мейне тогда обертка с вызовом подпроцесса, которая возвращает либо
    # значение ф-ии, либо поднимает exception, если процесс что-то записал в stderr
    args_str = ' '.join(str(arg) for arg in args_as_py_list)
    result = subprocess.run(command_to_run + ' ' + args_str, capture_output=True, text=True)
    if result.stderr:
        print(result.stderr)
        raise SubprocessError
    else:
        return float(result.stdout)

