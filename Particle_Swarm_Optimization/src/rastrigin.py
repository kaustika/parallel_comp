import sys
from math import cos, pi


def rastrigin(X):
    return 10*len(X) + sum(pow(x, 2) - 10*cos(2*pi*x) for x in X)


if __name__ == "__main__":
    print(rastrigin([float(arg) for arg in sys.argv[1:]]))
