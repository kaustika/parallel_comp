import sys


def rosenbrock(X):
    # a = 1, b = 100
    return pow(1 - X[0], 2) + 100 * pow((X[1] - pow(X[0], 2)), 2)


if __name__ == "__main__":
    print(rosenbrock([float(arg) for arg in sys.argv[1:]]))
