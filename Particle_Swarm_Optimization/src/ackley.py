from math import cos, pi, e, exp, sqrt
import sys


def ackley(X):
    return -20*exp(-0.2*sqrt(0.5 * (pow(X[0], 2) + pow(X[1], 2)))) - \
           exp(0.5*(cos(2*pi*X[0]) + cos(2*pi*X[1]))) + e + 20



if __name__ == "__main__":
    print(ackley([float(arg) for arg in sys.argv[1:]]))

