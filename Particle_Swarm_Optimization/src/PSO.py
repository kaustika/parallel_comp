from dataclasses import dataclass
from dataclasses_json import dataclass_json
from numpy.random import uniform
from numpy import argmin
from typing import Callable
from collections import deque
import time


@dataclass_json
@dataclass
class Bounds:
    lower: float
    upper: float


class Particle:
    def __init__(self, parameter_bounds: list[Bounds]):
        pos_lower_bounds = [bound.lower for bound in parameter_bounds]
        pos_upper_bounds = [bound.upper for bound in parameter_bounds]
        self.position = uniform(pos_lower_bounds, pos_upper_bounds)
        self.p_best = self.position
        self.f_p_best = None  # f(p_best)
        vel_lower_bounds = [-abs(bound.upper - bound.lower) for bound in parameter_bounds]
        vel_upper_bounds = [abs(bound.upper - bound.lower) for bound in parameter_bounds]
        self.velocity = uniform(vel_lower_bounds, vel_upper_bounds)

    def update_position(self, position: list[float]):
        self.position = position

    def update_velocity(self, velocity: list[float]):
        self.velocity = velocity


class Swarm:
    def __init__(self,
                 parameter_bounds: list[Bounds],
                 function: Callable[[list], float],
                 num_particles: int,
                 w: float,
                 c_1: int, c_2: int,
                 pool_size: int,
                 pool,
                 max_iter: int = 150,
                 exit_criteria_max_iter=False):
        self.pool_size = pool_size
        self.pool = pool
        self.max_iter = max_iter
        self.exit_criteria_max_iter = exit_criteria_max_iter
        self.time = None
        self.function = function
        self.iter_number = 0
        self.w = w  # momentum coefficient
        self.c_1 = c_1  # bias towards global best positions
        self.c_2 = c_2  # bias towards personal best positions
        self.values = []
        self.particles = []
        for _ in range(num_particles):
            self.particles.append(Particle(parameter_bounds))

        self.evaluate_function(self.particles)
        for i in range(len(self.particles)):
            p = self.particles[i]
            p.f_p_best = self.values[i]
        self.g_best = self.particles[argmin(self.values)].position
        self.f_g_best = min(self.values)  # f(g_best)

    def evaluate_function(self, particles):
        positions = [particle.position for particle in particles]
        self.values = self.pool.map(self.function, positions)

    def opt_iter(self):
        for p in self.particles:
            r_1 = uniform(0, 1)
            r_2 = uniform(0, 1)

            p.update_velocity(self.w * p.velocity +
                              self.c_1 * r_1 * (p.p_best - p.position) +
                              self.c_2 * r_2 * (self.g_best - p.position))
            p.update_position(p.position + p.velocity)

        self.evaluate_function(self.particles)

        for i in range(len(self.particles)):
            p = self.particles[i]
            if self.values[i] < p.f_p_best:
                p.p_best = p.position
                p.f_p_best = self.values[i]
                if self.values[i] < self.f_g_best:
                    self.g_best = p.position
                    self.f_g_best = self.values[i]

    def optimize(self):
        f_g_bests = []
        start = time.time()
        last_iters = deque(maxlen=20)
        last_iters.extend([1, 2, 3])
        while True:
            self.opt_iter()
            f_g_bests.append(self.f_g_best)
            last_iters.append(self.f_g_best)
            if self.iter_number > self.max_iter and self.exit_criteria_max_iter:
                # print(self.f_g_best)
                break
            if not self.exit_criteria_max_iter and len(set(last_iters))==1:
                # the value of the function stays the same for 20 iters
                # print(self.f_g_best)
                break
            self.iter_number += 1
            print(self.iter_number)
        end = time.time()
        self.time = end - start
        return self.g_best, f_g_bests
