import csv
from matplotlib import pyplot as plt


def visualize_iter_value_graph(f_g_bests: list[float], name: str):
    num_iters = len(f_g_bests)
    plt.plot(list(range(1, num_iters + 1)), f_g_bests, '-o')
    plt.xlabel('Итерация')
    plt.ylabel(f'Значение функции {name}')
    plt.title('Зависимость значения функции от итерации')
    plt.savefig(name + '.png')
    plt.clf()
    # plt.show()
    # plt.draw()


def save_stats_to_csv(cfg_name: str,
                      pool_size: int,
                      iters: int,
                      f_g_best: float,
                      g_best: float,
                      time: float,
                      num_particles: int,
                      w: float,
                      c_1: float,
                      c_2: float,
                      csv_name: str):
    with open(csv_name + '.csv', 'a', newline='') as stats:
        writer = csv.writer(stats)
        writer.writerow([cfg_name, pool_size, iters, f_g_best, g_best, time,
                         num_particles, w, c_1, c_2])
