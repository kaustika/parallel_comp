#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <omp.h>
#include <time.h>

double smoothness_level = 0;
double t = 10;
double delta_t = 0.2;
double k = 10;

typedef struct pgm {
    int w;
    int h;
    int max;
    int** p_data;
} pgm;

int** allocate_matrix_mem_int(int h, int w){
    int* data = (int*)malloc(sizeof(int)*h*w);
    int** matrix = (int**)malloc(sizeof(int*)*h);

    for (int i = 0; i < h ; i++) {
        matrix[i] = &(data[w*i]);
    }
    return matrix;
}

pgm* read_pgm(const char* filename){
    pgm* p_pgm;
    FILE* ifp;
    int word;
    char read_chars[256];

    //open the file, check if successful
    ifp = fopen( filename, "r" );
    if (!ifp) {
        printf("Error: Unable to open file %s.\n\n", filename);
        exit(1);
    }

    p_pgm = (pgm *) malloc (sizeof(pgm));

    //read headers from file
    printf ("Reading PGM file: %s...\n", filename);
    fscanf (ifp, "%s", read_chars);

    if (strcmp(read_chars, "P2") == 0) {
        //valid file type
        //get a word from the file
        printf("VALID TYPE.\n");
        fscanf (ifp, "%s", read_chars);
        while (read_chars[0] == '#') {
            //if a comment, get the rest of the line and a new word
            fgets (read_chars, 255, ifp);
            fscanf (ifp, "%s", read_chars);
        }

        //get width, height, color depth
        sscanf (read_chars, "%d", &p_pgm->w);
        fscanf (ifp, "%d", &p_pgm->h);
        fscanf (ifp, "%d", &p_pgm->max);
        printf("WIDTH: %d, HEIGHT: %d\n", p_pgm->w, p_pgm->h);

        p_pgm->p_data = allocate_matrix_mem_int(p_pgm->h, p_pgm->w);

        for (int i = 0; i < p_pgm->h; i++)
            for (int j = 0; j < p_pgm->w; j++){
                fscanf(ifp, "%d" ,&word);
                p_pgm->p_data[i][j] = word;
            }

        printf ("Loaded PGM. Size: %dx%d, Greyscale: %d \n",
                p_pgm->w, p_pgm->h, p_pgm->max + 1);
    }
    else {
        printf ("Error: %s. Format unsupported.\n\n", read_chars);
        exit(1);
    }
    fclose(ifp);

    return p_pgm;
}


double** allocate_matrix_mem_double(int h, int w){
    double* data = (double*)calloc(h*w, sizeof(double));
    double** matrix = (double**)calloc(h, sizeof(double*));

    for (int i = 0; i < h ; i++) {
        matrix[i] = &(data[w*i]);
    }
    return matrix;
}

void free_mem_double(double** matrix){
    free(matrix[0]);
    free(matrix);
}

void free_mem_int(int** matrix){
    free(matrix[0]);
    free(matrix);
}

void deepcopy(double** matrix, double** copy,int h, int w){
    for (int i = 0; i < h; i++)
        for (int j = 0; j < w; j++){
            copy[i][j] = matrix[i][j];
        }
}

void deepcopy_image(int** matrix, double** copy,int h, int w){
    for (int i = 1; i < h - 1; i++)
        for (int j = 1; j < w - 1; j++){
            copy[i][j] = matrix[i - 1][j - 1];
        }
}

int max_intensity(double** U, int h, int w) {
    double max_val = 0;
    for (int i = 1; i < h-1; i++) {
        for (int j = 1; j < w-1; j++) {
            if (U[i][j] > max_val) {
                max_val = U[i][j];
            }
        }
    }
    return (int)max_val;
}


void save_smoothed_matrix(double** U, int h, int w, const char* name){
    FILE* ndds;
    ndds = fopen(name, "wb");
    fprintf(ndds, "P2\n");
    fprintf(ndds, "%d %d\n", w-2, h-2);
    fprintf(ndds, "%d\n", max_intensity(U, h, w));
    for (int i = 1; i < h - 1; i++) {
        for (int j = 1; j < w - 1; j++) {
            fprintf(ndds, "%d ", (int)U[i][j]);
        }
        fprintf(ndds, "\n");
    }
    fclose(ndds);
}

void save_image_matrix(int** image, int h, int w, const char* name){

    FILE* gwdt;
    gwdt = fopen(name, "wb");
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            fprintf(gwdt, "%d ", (int)image[i][j]);
        }
        fprintf(gwdt, "\n");
    }
    fclose(gwdt);
}

void print_matrix_int(int** matrix, int h, int w){
    printf("\n");
    for(int i=0; i<10; i++){
        for(int j=0; j<10; j++){
            printf("%d ", matrix[i][j]);
        };
        printf("\n");
    };
};

void print_matrix_double(double** matrix, int h, int w){
    printf("\n");
    for(int i=1; i<h-1; i++){
        for(int j=1; j<w-1; j++){
            printf("%f ", matrix[i][j]);
        };
        printf("\n");
    };
};

double g(double x){
    return 1/(1 + pow(x/k, 2));
};


void iter_for_domain(double** U, double** U_prev, int w, int start, int stop){
    double N, S, E, W;
    int i, j;

    #pragma omp parallel for shared(U, U_prev) private(i, j, N, S, E, W)
    for (i = start; i < stop; i++){
        for (j = 1; j < w - 1; j++){
            N = U_prev[i - 1][j] - U_prev[i][j];
            S = U_prev[i + 1][j] - U_prev[i][j];
            E = U_prev[i][j + 1] - U_prev[i][j];
            W = U_prev[i][j - 1] - U_prev[i][j];

            U[i][j] = U_prev[i][j] +
                      delta_t * ( g(abs(N)) * N + g(abs(S)) * S +
                                  g(abs(E)) * E + g(abs(W)) * W);
        };
    };
    smoothness_level += delta_t;
    printf("Current smoothness level = %f\n", smoothness_level);
};

int main(int argc, char * argv[]){

    int num_threads = strtol(argv[1], NULL, 10);
    num_threads = num_threads > 0? num_threads: 1;
    omp_set_num_threads(num_threads);
    printf("Started calculation using %d threads\n", num_threads);

    double** U;
    double** U_prev;
    double** temp;
    int** image;
    int h, w, h_image, w_image;
    int start, stop;
    int iter_counter = 0;
    clock_t time_start;
    const char* filename =  argv[2];
    //const char* filename = "D:\\current_work\\POLY\\PAR_COMP\\NDDS_OMP\\everest.jpg";
    pgm* p_pgm;
    char command[100];
    sprintf(command, "convert %s -colorspace gray -compress none NDDS.pgm", filename);
    int status = system(command);

    // ������ PGM
    p_pgm = read_pgm("NDDS.pgm");
    image = p_pgm->p_data;
    h_image = p_pgm->h;
    w_image = p_pgm->w;
    // ����������� �������������� ����� ���������� 0-�� ����� �����
    h = h_image + 2;
    w = w_image + 2;

    // ��������� � ��������� �������
    // ��� ����� calloc
    U = allocate_matrix_mem_double(h, w);
    deepcopy_image(image, U, h, w);

    U_prev = allocate_matrix_mem_double(h, w);
    deepcopy(U, U_prev, h, w);

    // ������ [start, stop)
    start = 1;
    stop = h - 1;

    time_start = clock();
    // ������ ��� �������
    while( smoothness_level < t ){
        iter_for_domain(U, U_prev, w, start, stop);
        temp = U_prev;
        U_prev = U;
        U = temp;
        iter_counter++;
    };
    double time_taken = ((double)(clock() - time_start))/CLOCKS_PER_SEC;

    printf("\nIt took %d iterations and %f seconds.\n", iter_counter, time_taken);
    save_smoothed_matrix(U, h, w, "NDDS.pgm");
    printf("Fragment of the final smoothed image matrix: ");
    print_matrix_double(U, 10, 10);


    free_mem_int(image);
    free_mem_double(U);

    status = system("convert NDDS.pgm -colorspace gray -compress none NDDS.jpg");
    if (!status) printf("Smoothed image is NDDS.jpg. Check it out!\n");

    system("pause");
    return 0;
    };

