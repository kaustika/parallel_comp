#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include "mpi.h"

MPI_Status status;
double smoothness_level = 0;
double* convergence_flags;
double t = 10;
double delta_t = 0.2;
double k = 10;

typedef struct pgm {
    int w;
    int h;
    int max;
    int** p_data;
} pgm;

enum ERR
{
    E_TOO_MANY_PROCESSES = -1,
    E_IMAGE_TOO_SMALL = -2,
};

int** allocate_matrix_mem_int(int h, int w){
    int* data = (int*)malloc(sizeof(int)*h*w);
    int** matrix = (int**)malloc(sizeof(int*)*h);

    for (int i = 0; i < h ; i++) {
        matrix[i] = &(data[w*i]);
    }
    return matrix;
}

pgm* read_pgm(const char* filename){
    pgm* p_pgm;
    FILE* ifp;
    int word;
    char read_chars[256];

    //open the file, check if successful
    ifp = fopen( filename, "r" );
    if (!ifp) {
        printf("Error: Unable to open file %s.\n\n", filename);
        exit(1);
    }

    p_pgm = (pgm *) malloc (sizeof(pgm));

    //read headers from file
    printf ("Reading PGM file: %s...\n", filename);
    fscanf (ifp, "%s", read_chars);

    if (strcmp(read_chars, "P2") == 0) {
        //valid file type
        //get a word from the file
        printf("VALID TYPE.\n");
        fscanf (ifp, "%s", read_chars);
        while (read_chars[0] == '#') {
            //if a comment, get the rest of the line and a new word
            fgets (read_chars, 255, ifp);
            fscanf (ifp, "%s", read_chars);
        }

        //get width, height, color depth
        sscanf (read_chars, "%d", &p_pgm->w);
        fscanf (ifp, "%d", &p_pgm->h);
        fscanf (ifp, "%d", &p_pgm->max);
        printf("WIDTH: %d, HEIGHT: %d\n", p_pgm->w, p_pgm->h);

        p_pgm->p_data = allocate_matrix_mem_int(p_pgm->h, p_pgm->w);

        for (int i = 0; i < p_pgm->h; i++)
            for (int j = 0; j < p_pgm->w; j++){
                fscanf(ifp, "%d" ,&word);
                p_pgm->p_data[i][j] = word;
            }

        printf ("Loaded PGM. Size: %dx%d, Greyscale: %d \n",
                p_pgm->w, p_pgm->h, p_pgm->max + 1);
    }
    else {
        printf ("Error: %s. Format unsupported.\n\n", read_chars);
        exit(1);
    }
    fclose(ifp);

    return p_pgm;
}


bool is_source(int i_image, int j_image, int** wave_source_points, int num_sources){
   for (int j = 0; j < num_sources; j++){
        int i_source = wave_source_points[0][j];
        int j_source = wave_source_points[1][j];
        if (i_image == i_source && j_image == j_source)
            return true;
   };
   return false;
}



double** allocate_matrix_mem_double(int h, int w){
    double* data = (double*)calloc(h*w, sizeof(double));
    double** matrix = (double**)calloc(h, sizeof(double*));

    for (int i = 0; i < h ; i++) {
        matrix[i] = &(data[w*i]);
    }
    return matrix;
}

void free_mem_double(double** matrix){
    free(matrix[0]);
    free(matrix);
}

void free_mem_int(int** matrix){
    free(matrix[0]);
    free(matrix);
}

void deepcopy(double** matrix, double** copy,int h, int w){
    for (int i = 0; i < h; i++)
        for (int j = 0; j < w; j++){
            copy[i][j] = matrix[i][j];
        }
}

void deepcopy_image(int** matrix, double** copy,int h, int w){
    for (int i = 1; i < h - 1; i++)
        for (int j = 1; j < w - 1; j++){
            copy[i][j] = matrix[i - 1][j - 1];
        }
}

double min(const double* arr, int length){
    double min_val = INFINITY;
    for (int i = 0; i < length; i++){
        if (arr[i] < min_val){
            min_val = arr[i];
        }
    }
    return min_val;
}

int max(const int* arr, int length){
    int max_val = arr[0];
    for (int i = 0; i < length; i++){
        if (arr[i] > max_val){
            max_val = arr[i];
        }
    }
    return max_val;
}

char* concat(const char *s1, const char *s2){
    char *result = malloc(strlen(s1) + strlen(s2) + 1); // +1 for the null-terminator
    // in real code you would check for errors in malloc here
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

int max_intensity(double** U, int h, int w) {
    double max_val = 0;
    for (int i = 1; i < h-1; i++) {
        for (int j = 1; j < w-1; j++) {
            if (U[i][j] > max_val) {
                max_val = U[i][j];
            }
        }
    }
    return (int)max_val;
}

void save_smoothed_matrix(double** U, int h, int w, const char* name){

    FILE* gwdt;
    gwdt = fopen(name, "wb");
    fprintf(gwdt, "P2\n");
    fprintf(gwdt, "%d %d\n", w-2, h-2);
    fprintf(gwdt, "%d\n", max_intensity(U, h, w));
    for (int i = 1; i < h - 1; i++) {
        for (int j = 1; j < w - 1; j++) {
            fprintf(gwdt, "%d ", (int)U[i][j]);
        }
        fprintf(gwdt, "\n");
    }
    fclose(gwdt);
}

void save_image_matrix(int** image, int h, int w, const char* name){

    FILE* gwdt;
    gwdt = fopen(name, "wb");
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            fprintf(gwdt, "%d ", (int)image[i][j]);
        }
        fprintf(gwdt, "\n");
    }
    fclose(gwdt);
}

int get_domain_start(int rank, int num_proc, int num_lines, int h){
    int start; // ������ [start, stop)

    if (rank == 0){
        start = 1; // 2 ������� ����� ������ ������� ��� GWDT, ��� ��� ����� 5x5
    }
    if (rank > 0 && rank < num_proc - 1){
        // ���� �� ������� �� 0 � �� ���������
        start = rank * num_lines + (h - 2)%num_lines + 1;
    }
    if (rank == num_proc - 1 && rank != 0){
          // ���� �� ��������� ������� � �� ������������
          start = h - 1 - num_lines;
    };
    return start;
};

int get_domain_stop(int rank, int num_proc, int num_lines, int h){

    int start, stop; // ������ [start, stop)

    if (rank == 0){
        start = 1; // 2 ������� ����� ������ ������� ��� GWDT, ��� ��� ����� 5x5
        stop = start + num_lines;


        printf("\n stop %d", stop);
        printf("\n num_lines %d", num_lines);
    };
    if (rank > 0 && rank < num_proc - 1){
        // ���� �� ������� �� 0 � �� ���������
        start = rank * num_lines + (h-2)%num_lines + 1;
        stop = start + num_lines;
    }
    if (rank == num_proc - 1 && rank != 0){
          // ���� �� ��������� ������� � �� ������������
          stop = h - 1;
    };
    return stop;
};

void print_matrix_int(int** matrix, int h, int w){
    printf("\n");
    for(int i=0; i<10; i++){
        for(int j=0; j<10; j++){
            printf("%d ", matrix[i][j]);
        };
        printf("\n");
    };
};

void print_matrix_double(double** matrix, int h, int w){
    printf("\n");
    for(int i=1; i<h-1; i++){
        for(int j=1; j<w-1; j++){
            printf("%f ", matrix[i][j]);
        };
        printf("\n");
    };
};

bool are_all_finished(int size){
    bool flag = true;
    for (int i = 0; i < size; i++){
        if (convergence_flags[i] < t) {
            flag = false;
            break;
        };
    };
    return flag;
};

bool are_equal_func(double** U, double** U_prev, int h, int w){
    bool flag = true;
    int i,j;
    for (i = 0; i < h; i++)
        for (j = 0; j < w; j++){
            if (U[i][j] != U_prev[i][j]){
                flag = false;
                return flag;
            };
        };
    return flag;
};

double g(double x){
    return 1/(1 + pow(x/k, 2));
};


void iter_for_domain(double** U, double** U_prev, int w, int rank, int num_proc, int start, int stop){
    double N, S, E, W;
    int data_tag = 0;

    if (smoothness_level < t){
        for (int i = start; i < stop; i++){
            for (int j = 1; j < w - 1; j++){
                N = U_prev[i - 1][j] - U_prev[i][j];
                S = U_prev[i + 1][j] - U_prev[i][j];
                E = U_prev[i][j + 1] - U_prev[i][j];
                W = U_prev[i][j - 1] - U_prev[i][j];

                U[i][j] = U_prev[i][j] +
                          delta_t * ( g(abs(N)) * N + g(abs(S)) * S +
                                      g(abs(E)) * E + g(abs(W)) * W);
            };
       };
       smoothness_level += delta_t;
    };


    // ������ ���������� U ������
    if (rank == 0 && num_proc >= 2){
    // ���������� �������, �������� �� �������
    // ����� ����� ������ ���� ��� �� �������
        if ( convergence_flags[0] < t && convergence_flags[1] < t ){
            MPI_Sendrecv(&(U[stop - 1][0]), w, MPI_DOUBLE, 1, data_tag,
                         &(U[stop][0]), w, MPI_DOUBLE, 1, data_tag,
                         MPI_COMM_WORLD, &status);
        };

    };
    if (rank == num_proc - 1 && rank != 0){
        // ���������� ��������������, �������� �� ��������������
        if ( convergence_flags[rank] < t && convergence_flags[rank - 1] < t){
            MPI_Sendrecv(&(U[start][0]), w, MPI_DOUBLE, rank - 1, data_tag,
                         &(U[start - 1][0]), w, MPI_DOUBLE, rank - 1, data_tag,
                         MPI_COMM_WORLD, &status);
        };
    };
    if (rank > 0 && rank < num_proc - 1){
        // ���������� ��������, ��������� �� ��������
        if ( convergence_flags[rank] < t && convergence_flags[rank - 1] < t){
            MPI_Sendrecv(&(U[start][0]), w, MPI_DOUBLE, rank - 1, data_tag,
                         &(U[start - 1][0]), w, MPI_DOUBLE, rank - 1, data_tag,
                         MPI_COMM_WORLD, &status);
        };

        // ���������� �������, ��������� �� �������
        if ( convergence_flags[rank] < t && convergence_flags[rank + 1] < t){
            MPI_Sendrecv(&(U[stop - 1][0]), w, MPI_DOUBLE, rank + 1, data_tag,
                         &(U[stop][0]), w, MPI_DOUBLE, rank + 1, data_tag, MPI_COMM_WORLD, &status);
        };
    };

    // ������ ��������� ����������
    if (rank == 0){
        printf("Smoothness level for each domain at current iteration: \n");
        for (int i = 0; i < num_proc; i++) printf(" %.2f", convergence_flags[i]);
    };
    MPI_Allgather(&smoothness_level, 1, MPI_DOUBLE, &(convergence_flags[0]), 1, MPI_DOUBLE, MPI_COMM_WORLD);
    if (rank == 0){
        printf("\n");
        for (int i = 0; i < num_proc; i++) printf(" %.2f", convergence_flags[i]);
        printf("\n");
    };
    // ������������� �������, �� MPI_Allgather -> ������ ����� ����� �� �����
};

int main(int argc, char * argv[]){
    // ��� ���� ��������� �����
    double** U;
    double** U_prev;
    double** temp;
    int h, w, num_lines;
    int start, stop;
    int iter_counter = 0;

    int num_proc, rank_proc;
    int data_tag = 0;


    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &num_proc);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank_proc);


    convergence_flags = (double*)malloc(num_proc*sizeof(double));
    for (int i = 0; i < num_proc; i++) convergence_flags[i] = 0;

    printf("MPI initialized!");
    printf("Im proc %d!", rank_proc);

    if (0 == rank_proc){
        printf("Im proc 0!");
        int** image;
        int h_image, w_image;
        const char* filename =  argv[1];
        //const char* filename = "D:\\current_work\\POLY\\PAR_COMP\\NDDS_MPI\\everest.jpg";
        char command[100];
        sprintf(command, "convert %s -colorspace gray -compress none NDDS.pgm", filename);
        system(command);

        pgm* p_pgm;

        // ������ PGM
        p_pgm = read_pgm("NDDS.pgm");
        image = p_pgm->p_data;
        h_image = p_pgm->h;
        w_image = p_pgm->w;
        // ����������� �������������� ����� ���������� 0-�� ����� �����
        h = h_image + 2;
        w = w_image + 2;

        //������ ����� ��������, ���� ��������� ����� ��������� ������������
        if (ceil(h/3) < num_proc){ // ����� ��������� ������, ��� ��������� ������ �����������
            printf("Please set number of processes <= %d ", (int)ceil(h/3));
            MPI_Finalize();
            return E_TOO_MANY_PROCESSES;
        };

        // ��������� � ��������� �������
        // ��� ����� calloc
        U = allocate_matrix_mem_double(h, w);
        deepcopy_image(image, U, h, w);
        free_mem_int(image);

        // �������, ���� ������� ����� ������������
        int num_lines_per_block = (h-2)/num_proc; // ��� ���� ����� �� �������
        int modulo = (h-2)%num_lines_per_block;
        num_lines = modulo == 0? num_lines_per_block: num_lines_per_block + modulo; // ��� �������

        if (num_proc >= 2){
            for (int i = 1; i < num_proc; i++){
                MPI_Send(&h, 1, MPI_INT, i, data_tag, MPI_COMM_WORLD);
                MPI_Send(&w, 1, MPI_INT, i, data_tag, MPI_COMM_WORLD);
                MPI_Send(&(U[0][0]), h*w, MPI_DOUBLE, i, data_tag, MPI_COMM_WORLD);
                MPI_Send(&num_lines_per_block, 1, MPI_INT, i, data_tag, MPI_COMM_WORLD);
            };
        };
    };
    if (rank_proc > 0){
        // ��������� �� �������� ��������
        MPI_Recv(&h, 1, MPI_INT, 0, data_tag, MPI_COMM_WORLD, &status);
        MPI_Recv(&w, 1, MPI_INT, 0, data_tag, MPI_COMM_WORLD, &status);
        U = allocate_matrix_mem_double(h, w);
        MPI_Recv(&(U[0][0]), h*w, MPI_DOUBLE, 0, data_tag, MPI_COMM_WORLD, &status);
        MPI_Recv(&num_lines, 1, MPI_INT, 0, data_tag, MPI_COMM_WORLD, &status);
        printf("Process %d recieved matrices U0(%d x %d), image(%d x %d) and number of lines it has to process = %d", rank_proc, h, w, h, w, num_lines);
        // ������ � ������� �������� ���� ���� ����� ������� U � �� �����, ������� ����� ����� � ��� ������
        // ������ �� ������ ����� � ���������� �����, ������ ������ ��������� � ����� � �� ����� ������ ��� ����
    };

    // ������ ���������� ������ � ����� ������ ����� � num_lines �����
    // ������ [start, stop)
    start = get_domain_start(rank_proc, num_proc, num_lines, h);
    stop = get_domain_stop(rank_proc, num_proc, num_lines, h);

    printf("\n Im proc %d, start, stop = [%d, %d)\n", rank_proc, start, stop);

    U_prev = allocate_matrix_mem_double(h, w);
    deepcopy(U, U_prev, h, w);

    // ������ ��� �������
    while(  !are_all_finished(num_proc) ){
        iter_for_domain(U, U_prev, w, rank_proc, num_proc, start, stop);
        temp = U_prev;
        U_prev = U;
        U = temp;
        iter_counter++;
    };

    // ������ ������ ������ �������, ����� ����� ���� ������� ��������� NDDS-�������������� ����� �����������
    // ������������� �������, ��� ��� � ����� ������ �������� All_gather

    if (0 == rank_proc){
        // ��������� ��� ����� ��� ���� - for
        // ���������� �� ����� ���� ���� � �������
        // ���������� final ������� � ����

        for (int i = 1; i < num_proc; i++){
            // ������� start �� ������� �������� -
            MPI_Recv(&start, 1, MPI_INT, i, data_tag, MPI_COMM_WORLD, &status);
            // ������� ������ �� �������
            MPI_Recv(&(U[start][0]), ((h-2)/num_proc) * w, MPI_DOUBLE, i, data_tag, MPI_COMM_WORLD, &status);
        };
        // ����� ����� for � U ������ ���� ��� ��� ����������� �������� ��� �����
        printf("\nIt took %d iterations.\n", iter_counter);
        save_smoothed_matrix(U, h, w, "NDDS.pgm");
        printf("\n Fragment of the final smoothed image matrix: ");
        print_matrix_double(U, 10, 10);

        int status_pgm = system("convert NDDS.pgm -colorspace gray -compress none NDDS.jpg");
        if (!status_pgm) printf("Smoothed image is NDDS.jpg. Check it out!\n");
    }
    else{
        // ��������� ������� ��� ������� �������� - start and num_lines
        MPI_Send(& start, 1, MPI_INT, 0, data_tag, MPI_COMM_WORLD);
        // ��������� ���������� �� ����� ��� ����� ���� �������
        MPI_Send(&(U[start][0]), num_lines * w, MPI_DOUBLE, 0, data_tag, MPI_COMM_WORLD);
    };

    // ������
    free_mem_double(U);
    free(convergence_flags);

    MPI_Finalize();
    return 0;
};

