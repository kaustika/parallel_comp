#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include "mpi.h"


MPI_Status status;
int domain_finished = 0;
int* convergence_flags;

typedef struct pgm {
    int w;
    int h;
    int max;
    int** p_data;
} pgm;

enum ERR
{
    E_TOO_MANY_PROCESSES = -1,
    E_IMAGE_TOO_SMALL = -2,
};

int** allocate_matrix_mem_int(int h, int w){
    int* data = (int*)malloc(sizeof(int)*h*w);
    int** matrix = (int**)malloc(sizeof(int*)*h);

    for (int i = 0; i < h ; i++) {
        matrix[i] = &(data[w*i]);
    }
    return matrix;
}

pgm* read_pgm(const char* filename){
    pgm* p_pgm;
    FILE* ifp;
    int word;
    char read_chars[256];

    //open the file, check if successful
    ifp = fopen( filename, "r" );
    if (!ifp) {
        printf("Error: Unable to open file %s.\n\n", filename);
        exit(1);
    }

    p_pgm = (pgm *) malloc (sizeof(pgm));

    //read headers from file
    printf ("Reading PGM file: %s...\n", filename);
    fscanf (ifp, "%s", read_chars);

    if (strcmp(read_chars, "P2") == 0) {
        //valid file type
        //get a word from the file
        printf("VALID TYPE.\n");
        fscanf (ifp, "%s", read_chars);
        while (read_chars[0] == '#') {
            //if a comment, get the rest of the line and a new word
            fgets (read_chars, 255, ifp);
            fscanf (ifp, "%s", read_chars);
        }

        //get width, height, color depth
        sscanf (read_chars, "%d", &p_pgm->w);
        fscanf (ifp, "%d", &p_pgm->h);
        fscanf (ifp, "%d", &p_pgm->max);
        printf("WIDTH: %d, HEIGHT: %d\n", p_pgm->w, p_pgm->h);

        p_pgm->p_data = allocate_matrix_mem_int(p_pgm->h, p_pgm->w);

        for (int i = 0; i < p_pgm->h; i++)
            for (int j = 0; j < p_pgm->w; j++){
                fscanf(ifp, "%d" ,&word);
                p_pgm->p_data[i][j] = word;
            }

        printf ("Loaded PGM. Size: %dx%d, Greyscale: %d \n",
                p_pgm->w, p_pgm->h, p_pgm->max + 1);
    }
    else {
        printf ("Error: %s. Format unsupported.\n\n", read_chars);
        exit(1);
    }
    fclose(ifp);

    return p_pgm;
}

bool is_source(int i_image, int j_image, int** wave_source_points, int num_sources){
   for (int j = 0; j < num_sources; j++){
        int i_source = wave_source_points[0][j];
        int j_source = wave_source_points[1][j];
        if (i_image == i_source && j_image == j_source)
            return true;
   };
   return false;
}



double** allocate_matrix_mem_double(int h, int w){
    double* data = (double*)malloc(sizeof(double)*h*w);
    double** matrix = (double**)malloc(sizeof(double*)*h);

    for (int i = 0; i < h ; i++) {
        matrix[i] = &(data[w*i]);
    }
    return matrix;
}

void free_mem_double(double** matrix, int h){
    free(matrix[0]);
    free(matrix);
}

void free_mem_int(int** matrix, int h){
    free(matrix[0]);
    free(matrix);
}

void deepcopy(double** matrix, double** copy,int h, int w){
    for (int i = 0; i < h; i++)
        for (int j = 0; j < w; j++){
            copy[i][j] = matrix[i][j];
        }
}

double min(const double* arr, int length){
    double min_val = INFINITY;
    for (int i = 0; i < length; i++){
        if (arr[i] < min_val){
            min_val = arr[i];
        }
    }
    return min_val;
}

int max(const int* arr, int length){
    int max_val = arr[0];
    for (int i = 0; i < length; i++){
        if (arr[i] > max_val){
            max_val = arr[i];
        }
    }
    return max_val;
}

char* concat(const char *s1, const char *s2){
    char *result = malloc(strlen(s1) + strlen(s2) + 1); // +1 for the null-terminator
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

bool are_equal(double** U, double** U_prev, int h, int w){
    bool flag = true;
    int i,j;
    for (i = 0; i < h; i++)
        for (j = 0; j < w; j++){
            if (U[i][j] != U_prev[i][j]){
                flag = false;
                return flag;
            };
        };
    return flag;
}

int max_intensity(double** U, int h, int w) {
    double max_val = 0;
    for (int i = 1; i < h-1; i++) {
        for (int j = 1; j < w-1; j++) {
            if (U[i][j] > max_val) {
                max_val = U[i][j];
            }
        }
    }
    return (int)max_val;
}

void save_distance_matrix(double** scaled, int h, int w, const char* name){

    FILE* gwdt;
    int max_intens = max_intensity(scaled, h, w);
    gwdt = fopen(name, "wb");
    fprintf(gwdt, "P2\n");
    fprintf(gwdt, "%d %d\n", w, h);
    fprintf(gwdt, "%d\n", max_intens != 0 ? max_intens : 255);
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            fprintf(gwdt, "%d ", (int)scaled[i][j]);
        }
        fprintf(gwdt, "\n");
    }
    fclose(gwdt);
}

void save_image_matrix(int** image, int h, int w, const char* name){

    FILE* gwdt;
    gwdt = fopen(name, "wb");
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            fprintf(gwdt, "%d ", (int)image[i][j]);
        }
        fprintf(gwdt, "\n");
    }
    fclose(gwdt);
}

int get_domain_start(int rank, int num_proc, int num_lines, int h){
    int start; // строки [start, stop)

    if (rank == 0){
        start = 2; // 2 пикселя сбоку всегда остются без GWDT, так как рамка 5x5
    }
    if (rank > 0 && rank < num_proc - 1){
        // если мы процесс не 0 и не последний
        start = rank * num_lines + (h - 4)%num_lines + 2;
    }
    if (rank == num_proc - 1 && rank != 0){
          // если мы последний процесс и не единственный
          start = h - 2 - num_lines;
    };
    return start;
};

int get_domain_stop(int rank, int num_proc, int num_lines, int h){

    int start, stop; // строки [start, stop)

    if (rank == 0){
        start = 2; // 2 пикселя сбоку всегда остются без GWDT, так как рамка 5x5
        stop = start + num_lines;


        printf("\n stop %d", stop);
        printf("\n num_lines %d", num_lines);
    };
    if (rank > 0 && rank < num_proc - 1){
        // если мы процесс не 0 и не последний
        start = rank * num_lines + (h-4)%num_lines + 2;
        stop = start + num_lines;
    }
    if (rank == num_proc - 1 && rank != 0){
          // если мы последний процесс и не единственный
          stop = h - 2;
    };
    return stop;
};

void print_matrix_int(int** matrix, int h, int w){
    printf("\n");
    for(int i=0; i<10; i++){
        for(int j=0; j<10; j++){
            printf("%d ", matrix[i][j]);
        };
        printf("\n");
    };
};

void print_matrix_double(double** matrix, int h, int w){
    printf("\n");
    for(int i=0; i<h; i++){
        for(int j=0; j<w; j++){
            printf("%f ", matrix[i][j]);
        };
        printf("\n");
    };
};

bool are_all_finished(int size){
    bool flag = true;
    for (int i = 0; i < size; i++){
        if (!convergence_flags[i]) {
            flag = false;
            break;
        };
    };
    return flag;
};

bool are_equal_func(double** U, double** U_prev, int h, int w){
    bool flag = true;
    int i,j;
    for (i = 0; i < h; i++)
        for (j = 0; j < w; j++){
            if (U[i][j] != U_prev[i][j]){
                flag = false;
                return flag;
            };
        };
    return flag;
};

double** scale_matrix(double** U, int h, int w){
    // Create a new matrix of the same dimensions (excluding the border elements)
    int new_h = h - 4;  // exclude 2 rows from the top and 2 rows from the bottom
    int new_w = w - 4;  // exclude 2 columns from the left and 2 columns from the right
    double** scaled_matrix = allocate_matrix_mem_double(new_h, new_w);

    // Find the minimum and maximum values of U (excluding the border elements)
    double U_min = U[2][2];
    double U_max = U[2][2];
    for(int i = 2; i < h - 2; i++){    // exclude 2 rows from the top and 2 rows from the bottom
        for(int j = 2; j < w - 2; j++){  // exclude 2 columns from the left and 2 columns from the right
            if(U[i][j] < U_min) U_min = U[i][j];
            if(U[i][j] > U_max) U_max = U[i][j];
        }
    }

    // Scale the values of U (excluding the border elements) to [0, 255] and update the values in the new matrix
    double scale_factor = U_max - U_min != 0 ? 255 / (U_max - U_min): 255 / U_max;
    for(int i = 2; i < h - 2; i++){    // exclude 2 rows from the top and 2 rows from the bottom
        for(int j = 2; j < w - 2; j++){  // exclude 2 columns from the left and 2 columns from the right
            int x = i - 2;  // index in the new matrix
            int y = j - 2;  // index in the new matrix
            scaled_matrix[x][y] = (U[i][j] - U_min) * scale_factor;
        }
    }

    return scaled_matrix;
}

void iter_for_domain(double** U, double** U_prev, int** image, int h, int w, int rank, int num_proc, int start, int stop){
    int a = 1;
    double b = sqrt(2);
    double c = sqrt(5);
    int d = 2;
    double e = 2 * sqrt(2);
    int data_tag = 0;

    // перед началом новой итерации в клетках по бокам полос уже лежат нужные значения с предыдущей итерации
    if (!domain_finished){
        for (int i = 2; i <= h - 1; i++){
            for (int j = 2; j <= w - 3; j++){
                double arr[13] = {U[i - 1][j] + a * image[i][j],
                                  U[i][j - 1] + a * image[i][j],
                                  U[i - 1][j - 1] + b * image[i][j],
                                  U[i - 1][j + 1] + b * image[i][j],
                                  U[i - 1][j + 2] + c * image[i][j],
                                  U[i - 1][j - 2] + c * image[i][j],
                                  U[i - 2][j - 1] + c * image[i][j],
                                  U[i - 2][j + 1] + c * image[i][j],
                                  U[i - 2][j] + d * image[i][j],
                                  U[i][j - 2] + d * image[i][j],
                                  U[i - 2][j - 2] + e * image[i][j],
                                  U[i - 2][j + 2] + e * image[i][j],
                                  U_prev[i][j]};
                U[i][j] = min(arr, 13);
            };
       };

        for (int i = h - 3; i >= 0; i--) {
            for (int j = w - 3; j >= 2; j--) {
                 double arr[13] = {U[i + 1][j] + a * image[i][j],
                                   U[i][j + 1] + a * image[i][j],
                                   U[i + 1][j + 1] + b * image[i][j],
                                   U[i + 1][j - 1] + b * image[i][j],
                                   U[i + 1][j + 2] + c * image[i][j],
                                   U[i + 1][j - 2] + c * image[i][j],
                                   U[i + 2][j + 1] + c * image[i][j],
                                   U[i + 2][j - 1] + c * image[i][j],
                                   U[i][j + 2] + d * image[i][j],
                                   U[i + 2][j] + d * image[i][j],
                                   U[i + 2][j + 2] + e * image[i][j],
                                   U[i + 2][j - 2] + e * image[i][j],
                                   U_prev[i][j]};
                U[i][j] = min(arr, 13);
            };
        };

    };

    if (rank == 0 && num_proc >= 2){
    // отправляет первому, получает от первого
    // обмен нужен только если хоть один не сошелся
        if ( !convergence_flags[0] || !convergence_flags[1] ){
            MPI_Sendrecv(&(U[stop - 2][0]), 2*w, MPI_DOUBLE, 1, data_tag,
                     &(U[stop][0]), 2*w, MPI_DOUBLE, 1, data_tag,
                     MPI_COMM_WORLD, &status);
        };

    };
    if (rank == num_proc - 1 && rank != 0){
        // отправляет предпоследнему, получает от предпоследнего
        if ( !convergence_flags[rank] || !convergence_flags[rank - 1]){
            MPI_Sendrecv(&(U[start][0]), 2*w, MPI_DOUBLE, rank - 1, data_tag,
                     &(U[start - 2][0]), 2*w, MPI_DOUBLE, rank - 1, data_tag,
                     MPI_COMM_WORLD, &status);
        };
    };
    if (rank > 0 && rank < num_proc - 1){


        // отправляет верхнему, принимает от верхнего
        if ( !convergence_flags[rank] || !convergence_flags[rank - 1]){
            MPI_Sendrecv(&(U[start][0]), 2*w, MPI_DOUBLE, rank - 1, data_tag,
                     &(U[start - 2][0]), 2*w, MPI_DOUBLE, rank - 1, data_tag,
                     MPI_COMM_WORLD, &status);
        };

        // отпарвляет нижнему, принимает от нижнего
        if ( !convergence_flags[rank] || !convergence_flags[rank + 1]){
            MPI_Sendrecv(&(U[stop - 2][0]), 2*w, MPI_DOUBLE, rank + 1, data_tag,
                     &(U[stop][0]), 2*w, MPI_DOUBLE, rank + 1, data_tag, MPI_COMM_WORLD, &status);
        };
    };

    // после всех обменов для U(!), если мы сошлись, то domain_finished станет true
    domain_finished = are_equal_func(U, U_prev, h, w);

    //теперь сообщить всем, сошлись мы или нет + узнать у них - выход из while будет дружно ровно тогда, когда последний сойдется, а до этого
    //сошедшиеся хоть и не считают
    printf("\n Convergence NOT gathered!\n");
    for (int i = 0; i < num_proc; i++) printf(" %d", convergence_flags[i]);
    MPI_Allgather(&domain_finished, 1, MPI_INT, &(convergence_flags[0]), 1, MPI_INT, MPI_COMM_WORLD);
    printf("\n");
    for (int i = 0; i < num_proc; i++) printf(" %d", convergence_flags[i]);

};

int main(int argc, char * argv[]){
    // это всем процессам нужно
    double** U;
    double** temp;
    int** image;
    int h, w, num_lines;
    int start, stop;
    int iter_counter = 0;


    int num_proc, rank_proc;
    int data_tag = 0;


    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &num_proc);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank_proc);

    convergence_flags = (int*)malloc(num_proc*sizeof(int));
    for (int i = 0; i < num_proc; i++) convergence_flags[i] = 0;

    printf("MPI initialized!");
    printf("Im proc %d!", rank_proc);
    if (0 == rank_proc){
        printf("Im proc 0!");
        const char* filename =  argv[1];
        //const char* filename = "D:\\current_work\\POLY\\PAR_COMP\\GWDT_MPI\\chess.jpg";
        pgm* p_pgm;
        int** wave_source_points;
        int num_sources = 2;
        int dim = 2; //2d image

        char command[100];
        sprintf(command, "convert %s -colorspace gray -compress none GWDT.pgm", filename);
        system(command);

        p_pgm = read_pgm("GWDT.pgm");
        image = p_pgm->p_data;
        h = p_pgm->h;
        w = p_pgm->w;


        // можно выбрать источники по локальным или глобальному порогу
        // пусть источники будут на концах главной диагонали изображения
        // и хранятся в виде матрицы с координатами (источники однопиксельные).
        wave_source_points = allocate_matrix_mem_int(dim, num_sources);
        wave_source_points[0][0] = (int)h/2;
        wave_source_points[1][0] = (int)w/2;
        wave_source_points[0][1] = 0;
        wave_source_points[1][1] = w - 1;

        U = allocate_matrix_mem_double(h, w);
        //init
        for (int i = 0; i < h; i++){
            for (int j = 0; j < w; j++){
                // уточнить, сюда ли интенсивность/inf по вариантам
                U[i][j] = (is_source(i, j, wave_source_points, num_sources)) ? 0 : INFINITY;
            };
        };
        free_mem_int(wave_source_points, dim);

        if (ceil((h - 4)/5) < num_proc){ // попросили запустить выполнение в число процессов,
                                    // большее, чем позволяет размер изображения
            printf("Please set number of processes <= %d ", (int)ceil((h - 4)/5));
            free_mem_double(U, h);
            MPI_Finalize();
            return E_TOO_MANY_PROCESSES;
        };
        if (h < 5){// изображение слишком маленькое и вообще не можем обработать
            printf("Please choose an image with >= 5x5!");
            free_mem_double(U, h);
            MPI_Finalize();
            return E_TOO_MANY_PROCESSES;
        };


        // теперь процесс должен разослать всем приспешникам по целой матрице для простоты, тк как держат целиком
        int num_lines_per_block = (h - 4)/num_proc; // для всех остальных процессов
        int modulo = (h - 4)%num_lines_per_block;
        num_lines = modulo == 0? num_lines_per_block: num_lines_per_block + modulo; // для нулевого мб больше, если не делится
        if (num_proc >= 2){
            for (int i = 1; i < num_proc; i++){
                MPI_Send(&h, 1, MPI_INT, i, data_tag, MPI_COMM_WORLD);
                MPI_Send(&w, 1, MPI_INT, i, data_tag, MPI_COMM_WORLD);
                MPI_Send(&(U[0][0]), h*w, MPI_DOUBLE, i, data_tag, MPI_COMM_WORLD);
                MPI_Send(&(image[0][0]), h*w, MPI_INT, i, data_tag, MPI_COMM_WORLD);
                MPI_Send(&num_lines_per_block, 1, MPI_INT, i, data_tag, MPI_COMM_WORLD);
            };
        };
    };


    if (rank_proc > 0){
          // принимаем от нулевого процесса
            MPI_Recv(&h, 1, MPI_INT, 0, data_tag, MPI_COMM_WORLD, &status);
            MPI_Recv(&w, 1, MPI_INT, 0, data_tag, MPI_COMM_WORLD, &status);
            U = allocate_matrix_mem_double(h, w);
            MPI_Recv(&(U[0][0]), h*w, MPI_DOUBLE, 0, data_tag, MPI_COMM_WORLD, &status);
            image = allocate_matrix_mem_int(h, w);
            MPI_Recv(&(image[0][0]), h*w, MPI_INT, 0, data_tag, MPI_COMM_WORLD, &status);
            MPI_Recv(&num_lines, 1, MPI_INT, 0, data_tag, MPI_COMM_WORLD, &status);
            printf("Process %d recieved matrices U0(%d x %d), image(%d x %d) and number of lines it has to process = %d", rank_proc, h, w, h, w, num_lines);
            // теперь у каждого процесса есть своя копия матрицы U и он знает, сколько строк будет в его полосе
            // исходя из своего ранга и количества строк, каждый сможет посчитать с какой и до какой строки его блок


    };

    // определим начало и конец блока в num_lines строк
    // строки [start, stop)
    start = get_domain_start(rank_proc, num_proc, num_lines, h);
    stop = get_domain_stop(rank_proc, num_proc, num_lines, h);

    printf("\n Im proc %d, start, stop = [%d, %d]", rank_proc, start, stop);
    double** U_prev = allocate_matrix_mem_double(h, w);
    deepcopy(U, U_prev, h, w);
    // теперь ВСЕ считают
    while( !are_all_finished(num_proc) ){
        iter_for_domain(U, U_prev, image, h, w, rank_proc, num_proc, start, stop);
        iter_counter++;
        temp = U_prev;
        U_prev = U;
        U = temp;
    };

    if (0 == rank_proc){
        // принимаем все куски ото всех - for
        // записываем их сразу куда надо в матрице
        // записываем final матрицу в файл
        int* iters_per_block = (int*)malloc(num_proc*sizeof(int));
        iters_per_block[0] = iter_counter;

        for (int i = 1; i < num_proc; i++){
            MPI_Recv(&(iters_per_block[i]), 1, MPI_INT, i, data_tag, MPI_COMM_WORLD, &status);
            // принять границы от каждого процесса - start and num_lines
            MPI_Recv(&start, 1, MPI_INT, i, data_tag, MPI_COMM_WORLD, &status);
            MPI_Recv(&num_lines, 1, MPI_INT, i, data_tag, MPI_COMM_WORLD, &status);
            // принять полосы от каждого
            MPI_Recv(&(U[start][0]), num_lines*w, MPI_DOUBLE, i, data_tag, MPI_COMM_WORLD, &status);
        };
        // после этого for в U должны быть уже все посчитанные значения для полос
        for (int i = 0; i < num_proc; i++) printf("%d ", iters_per_block[i]);
        printf("It took this much iterations: %d \n", max(iters_per_block, num_proc));
        printf("\nSuccess!");
        free(iters_per_block);
    }
    else{
        printf("%d", iter_counter);
        MPI_Send(&iter_counter, 1, MPI_INT, 0, data_tag, MPI_COMM_WORLD);
        // отправить границы для каждого процесса - start and num_lines
        MPI_Send(& start, 1, MPI_INT, 0, data_tag, MPI_COMM_WORLD);
        MPI_Send(&num_lines, 1, MPI_INT, 0, data_tag, MPI_COMM_WORLD);
        // отправить результаты по блоку для сбора всей матрицы
        MPI_Send(&(U[start][0]), num_lines*w, MPI_DOUBLE, 0, data_tag, MPI_COMM_WORLD);
    };

    if (rank_proc == 0) {
        double** scaled = scale_matrix(U, h, w);
        //print_matrix_double(U, 10, 10);
        save_distance_matrix(scaled, h-4, w-4, "GWDT.pgm");
        printf("\n Fragment of the final distance image matrix: ");
        print_matrix_double(scaled, 10, 10);

        int status = system("convert GWDT.pgm -colorspace gray GWDT.jpg");
        if (!status) printf("Transformed image is GWDT.jpg. Check it out!\n");

        free_mem_double(scaled, h-4);
    };

    free_mem_int(image, h);
    free_mem_double(U, h);
    free(convergence_flags);

    MPI_Finalize();
    return 0;
    };

