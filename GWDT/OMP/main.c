#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <omp.h>

int domain_finished = 0;
int* convergence_flags;
int a = 1;
double b = sqrt(2);
double c = sqrt(5);
int d = 2;
double e = 2 * sqrt(2);

typedef struct pgm {
    int w;
    int h;
    int max;
    int** p_data;
} pgm;

enum ERR
{
    E_TOO_MANY_PROCESSES = -1,
    E_IMAGE_TOO_SMALL = -2,
};

int** allocate_matrix_mem_int(int h, int w){
    int* data = (int*)malloc(sizeof(int)*h*w);
    int** matrix = (int**)malloc(sizeof(int*)*h);

    for (int i = 0; i < h ; i++) {
        matrix[i] = &(data[w*i]);
    }
    return matrix;
}

pgm* read_pgm(const char* filename){
    pgm* p_pgm;
    FILE* ifp;
    int word;
    char read_chars[256];

    //open the file, check if successful
    ifp = fopen( filename, "r" );
    if (!ifp) {
        printf("Error: Unable to open file %s.\n\n", filename);
        exit(1);
    }

    p_pgm = (pgm *) malloc (sizeof(pgm));

    //read headers from file
    printf ("Reading PGM file: %s...\n", filename);
    fscanf (ifp, "%s", read_chars);

    if (strcmp(read_chars, "P2") == 0) {
        //valid file type
        //get a word from the file
        printf("VALID TYPE.\n");
        fscanf (ifp, "%s", read_chars);
        while (read_chars[0] == '#') {
            //if a comment, get the rest of the line and a new word
            fgets (read_chars, 255, ifp);
            fscanf (ifp, "%s", read_chars);
        }

        //get width, height, color depth
        sscanf (read_chars, "%d", &p_pgm->w);
        fscanf (ifp, "%d", &p_pgm->h);
        fscanf (ifp, "%d", &p_pgm->max);
        printf("WIDTH: %d, HEIGHT: %d\n", p_pgm->w, p_pgm->h);

        p_pgm->p_data = allocate_matrix_mem_int(p_pgm->h, p_pgm->w);

        for (int i = 0; i < p_pgm->h; i++)
            for (int j = 0; j < p_pgm->w; j++){
                fscanf(ifp, "%d" ,&word);
                p_pgm->p_data[i][j] = word;
            }

        printf ("Loaded PGM. Size: %dx%d, Greyscale: %d \n",
                p_pgm->w, p_pgm->h, p_pgm->max + 1);
    }
    else {
        printf ("Error: %s. Format unsupported.\n\n", read_chars);
        exit(1);
    }
    fclose(ifp);

    return p_pgm;
}

bool is_source(int i_image, int j_image, int** wave_source_points, int num_sources){
   for (int j = 0; j < num_sources; j++){
        int i_source = wave_source_points[0][j];
        int j_source = wave_source_points[1][j];
        if (i_image == i_source && j_image == j_source)
            return true;
   };
   return false;
}

bool are_all_finished(int size){
    bool flag = true;
    for (int i = 0; i < size; i++){
        if (!convergence_flags[i]) {
            flag = false;
            break;
        };
    };
    return flag;
};

double** allocate_matrix_mem_double(int h, int w){
    double* data = (double*)malloc(sizeof(double)*h*w);
    double** matrix = (double**)malloc(sizeof(double*)*h);

    for (int i = 0; i < h ; i++) {
        matrix[i] = &(data[w*i]);
    }
    return matrix;
}

void free_mem_double(double** matrix, int h){
    free(matrix[0]);
    free(matrix);
}

void free_mem_int(int** matrix, int h){
    free(matrix[0]);
    free(matrix);
}

void deepcopy(double** matrix, double** copy,int h, int w){
    for (int i = 0; i < h; i++)
        for (int j = 0; j < w; j++){
            copy[i][j] = matrix[i][j];
        }
}

double min(const double* arr, int length){
    double min_val = INFINITY;
    for (int i = 0; i < length; i++){
        if (arr[i] < min_val){
            min_val = arr[i];
        }
    }
    return min_val;
}

int max(const int* arr, int length){
    int max_val = arr[0];
    for (int i = 0; i < length; i++){
        if (arr[i] > max_val){
            max_val = arr[i];
        }
    }
    return max_val;
}

char* concat(const char *s1, const char *s2){
    char *result = malloc(strlen(s1) + strlen(s2) + 1); // +1 for the null-terminator
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

bool are_equal(double** U, double** U_prev, int h, int w){
    bool flag = true;
    int i,j;
    for (i = 0; i < h; i++)
        for (j = 0; j < w; j++){
            if (U[i][j] != U_prev[i][j]){
                flag = false;
                return flag;
            };
        };
    return flag;
}

int max_intensity(double** U, int h, int w) {
    double max_val = 0;
    for (int i = 1; i < h-1; i++) {
        for (int j = 1; j < w-1; j++) {
            if (U[i][j] > max_val) {
                max_val = U[i][j];
            }
        }
    }
    return (int)max_val;
}


void save_distance_matrix(double** scaled, int h, int w, const char* name){

    FILE* gwdt;
    int max_intens = max_intensity(scaled, h, w);
    gwdt = fopen(name, "wb");
    fprintf(gwdt, "P2\n");
    fprintf(gwdt, "%d %d\n", w, h);
    fprintf(gwdt, "%d\n", max_intens != 0 ? max_intens : 255);
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            fprintf(gwdt, "%d ", (int)scaled[i][j]);
        }
        fprintf(gwdt, "\n");
    }
    fclose(gwdt);
}

void save_image_matrix(double** image, int h, int w, const char* name){

    FILE* gwdt;
    gwdt = fopen(name, "wb");
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            fprintf(gwdt, "%f ", image[i][j]);
        }
        fprintf(gwdt, "\n");
    }
    fclose(gwdt);
}

int get_domain_start(int rank, int num_proc, int num_lines, int h){
    int start; // ������ [start, stop)

    if (rank == 0){
        start = 2; // 2 ������� ����� ������ ������� ��� GWDT, ��� ��� ����� 5x5
    }
    if (rank > 0 && rank < num_proc - 1){
        // ���� �� ������� �� 0 � �� ���������
        start = rank * num_lines + (h - 4)%num_lines + 2;
    }
    if (rank == num_proc - 1 && rank != 0){
          // ���� �� ��������� ������� � �� ������������
          start = h - 2 - num_lines;
    };
    return start;
};

int get_domain_stop(int rank, int num_proc, int num_lines, int h){

    int start, stop; // ������ [start, stop)

    if (rank == 0){
        start = 2; // 2 ������� ����� ������ ������� ��� GWDT, ��� ��� ����� 5x5
        stop = start + num_lines;


        printf("\n stop %d", stop);
        printf("\n num_lines %d", num_lines);
    };
    if (rank > 0 && rank < num_proc - 1){
        // ���� �� ������� �� 0 � �� ���������
        start = rank * num_lines + (h-4)%num_lines + 2;
        stop = start + num_lines;
    }
    if (rank == num_proc - 1 && rank != 0){
          // ���� �� ��������� ������� � �� ������������
          stop = h - 2;
    };
    return stop;
};

void print_matrix_int(int** matrix, int h, int w){
    printf("\n");
    for(int i=0; i<10; i++){
        for(int j=0; j<10; j++){
            printf("%d ", matrix[i][j]);
        };
        printf("\n");
    };
};

void print_matrix_double(double** matrix, int h, int w){
    printf("\n");
    for(int i=2; i<h-2; i++){
        for(int j=2; j<w-2; j++){
            printf("%f ", matrix[i][j]);
        };
        printf("\n");
    };
};


bool are_equal_func(double** U, double** U_prev, int h, int w){
    bool flag = true;
    int i,j;
    for (i = 0; i < h; i++)
        for (j = 0; j < w; j++){
            if (U[i][j] != U_prev[i][j]){
                flag = false;
                return flag;
            };
        };
    return flag;
};

double** scale_matrix(double** U, int h, int w){
    // Create a new matrix of the same dimensions (excluding the border elements)
    int new_h = h - 4;  // exclude 2 rows from the top and 2 rows from the bottom
    int new_w = w - 4;  // exclude 2 columns from the left and 2 columns from the right
    double** scaled_matrix = allocate_matrix_mem_double(new_h, new_w);

    // Find the minimum and maximum values of U (excluding the border elements)
    double U_min = U[2][2];
    double U_max = U[2][2];
    for(int i = 2; i < h - 2; i++){    // exclude 2 rows from the top and 2 rows from the bottom
        for(int j = 2; j < w - 2; j++){  // exclude 2 columns from the left and 2 columns from the right
            if(U[i][j] < U_min) U_min = U[i][j];
            if(U[i][j] > U_max) U_max = U[i][j];
        }
    }

    // Scale the values of U (excluding the border elements) to [0, 255] and update the values in the new matrix
    double scale_factor = U_max - U_min != 0 ? 255 / (U_max - U_min): 255 / U_max;
    for(int i = 2; i < h - 2; i++){    // exclude 2 rows from the top and 2 rows from the bottom
        for(int j = 2; j < w - 2; j++){  // exclude 2 columns from the left and 2 columns from the right
            int x = i - 2;  // index in the new matrix
            int y = j - 2;  // index in the new matrix
            scaled_matrix[x][y] = (U[i][j] - U_min) * scale_factor;
        }
    }

    return scaled_matrix;
}

void min_choice_forward(double** U, double** U_prev, int** image, double arr[], int i, int j){
    arr[0]  =  U[i - 1][j] + a * image[i][j];
    arr[1]  =  U[i][j - 1] + a * image[i][j];
    arr[2]  =  U[i - 1][j - 1] + b * image[i][j];
    arr[3]  =  U[i - 1][j + 1] + b * image[i][j];
    arr[4]  =  U[i - 1][j + 2] + c * image[i][j];
    arr[5]  =  U[i - 1][j - 2] + c * image[i][j];
    arr[6]  =  U[i - 2][j - 1] + c * image[i][j];
    arr[7]  =  U[i - 2][j + 1] + c * image[i][j];
    arr[8]  =  U[i - 2][j] + d * image[i][j];
    arr[9]  =  U[i][j - 2] + d * image[i][j];
    arr[10] =  U[i - 2][j - 2] + e * image[i][j];
    arr[11] =  U[i - 2][j + 2] + e * image[i][j];
    arr[12] =  U_prev[i][j];
};

void min_choice_backward(double** U, double** U_prev, int** image, double arr[], int i, int j){
    arr[0] = U[i + 1][j] + a * image[i][j];
    arr[1] = U[i][j + 1] + a * image[i][j];
    arr[2] = U[i + 1][j + 1] + b * image[i][j];
    arr[3] = U[i + 1][j - 1] + b * image[i][j];
    arr[4] = U[i + 1][j + 2] + c * image[i][j];
    arr[5] = U[i + 1][j - 2] + c * image[i][j];
    arr[6] = U[i + 2][j + 1] + c * image[i][j];
    arr[7] = U[i + 2][j - 1] + c * image[i][j];
    arr[8] = U[i][j + 2] + d * image[i][j];
    arr[9] = U[i + 2][j] + d * image[i][j];
    arr[10] = U[i + 2][j + 2] + e * image[i][j];
    arr[11] = U[i + 2][j - 2] + e * image[i][j];
    arr[12] = U_prev[i][j];

};


void iter_for_domain(double** U, double** U_prev, int** image, int h, int w){
    //#pragma omp parallel default(none) shared(U, U_prev, image, a, b, c, d, e, h, w)
    //#pragma omp single
    for (int i = 2; i <= h - 1; i++){
        for (int j = 2; j <= w - 3; j++){
    //#pragma omp task depend(out: U[i][j]) depend(in: U[i - 1][j], U[i][j - 1], U[i - 1][j - 1], U[i - 1][j + 1], U[i - 1][j + 2], U[i - 1][j - 2], U[i - 2][j - 1], U[i - 2][j + 1], U[i - 2][j], U[i][j - 2], U[i - 2][j - 2], U[i - 2][j + 2], U_prev[i][j]) default(none) shared(U, U_prev, image, a, b, c, d, e) firstprivate(i, j)
            {
                double arr[13] = {U[i - 1][j] + a * image[i][j],
                                  U[i][j - 1] + a * image[i][j],
                                  U[i - 1][j - 1] + b * image[i][j],
                                  U[i - 1][j + 1] + b * image[i][j],
                                  U[i - 1][j + 2] + c * image[i][j],
                                  U[i - 1][j - 2] + c * image[i][j],
                                  U[i - 2][j - 1] + c * image[i][j],
                                  U[i - 2][j + 1] + c * image[i][j],
                                  U[i - 2][j] + d * image[i][j],
                                  U[i][j - 2] + d * image[i][j],
                                  U[i - 2][j - 2] + e * image[i][j],
                                  U[i - 2][j + 2] + e * image[i][j],
                                  U_prev[i][j]};
                U[i][j] = min(arr, 13);
            }

        };
    };

    //#pragma omp parallel default(none) shared(U, U_prev, image, a, b, c, d, e, h, w)
    //#pragma omp single
    for (int i = h - 3; i >= 0; i--) {
        for (int j = w - 3; j >= 2; j--) {
    //#pragma omp task depend(out: U[i][j]) depend(in: U[i + 1][j], U[i][j + 1], U[i + 1][j + 1], U[i + 1][j - 1], U[i + 1][j + 2], U[i + 1][j - 2], U[i + 2][j + 1], U[i + 2][j - 1], U[i][j + 2], U[i + 2][j], U[i + 2][j + 2], U[i + 2][j - 2], U_prev[i][j]) default(none) shared(U, U_prev, image, a, b, c, d, e) firstprivate(i, j)
                {
                    double arr[13] = {U[i + 1][j] + a * image[i][j],
                                      U[i][j + 1] + a * image[i][j],
                                      U[i + 1][j + 1] + b * image[i][j],
                                      U[i + 1][j - 1] + b * image[i][j],
                                      U[i + 1][j + 2] + c * image[i][j],
                                      U[i + 1][j - 2] + c * image[i][j],
                                      U[i + 2][j + 1] + c * image[i][j],
                                      U[i + 2][j - 1] + c * image[i][j],
                                      U[i][j + 2] + d * image[i][j],
                                      U[i + 2][j] + d * image[i][j],
                                      U[i + 2][j + 2] + e * image[i][j],
                                      U[i + 2][j - 2] + e * image[i][j],
                                      U_prev[i][j]};
                    U[i][j] = min(arr, 13);
                }

        };
    };
    domain_finished = are_equal_func(U, U_prev, h, w);
};

int main(int argc, char * argv[]){

    int num_threads = strtol(argv[1], NULL, 10);
    num_threads = num_threads > 0? num_threads: 1;
    omp_set_num_threads(num_threads);
    printf("Started calculation using %d threads\n", num_threads);

    double** U;
    int** image;
    double** temp;
    int h, w;
    int iter_counter = 0;

    const char* filename =  argv[2];
    //const char* filename = "D:\\current_work\\POLY\\PAR_COMP\\GWDT_OMP\\chess.jpg";
    pgm* p_pgm;
    int** wave_source_points;
    int num_sources = 2;
    int dim = 2; //2d image

    char command[100];
    sprintf(command, "convert %s -colorspace gray -compress none GWDT.pgm", filename);
    system(command);


    p_pgm = read_pgm("GWDT.pgm");
    image = p_pgm->p_data;
    h = p_pgm->h;
    w = p_pgm->w;


    // ����� ������� ��������� �� ��������� ��� ����������� ������
    // ����� ��������� ����� �� ������ ������� ��������� �����������
    // � �������� � ���� ������� � ������������ (��������� ��������������).

    wave_source_points = allocate_matrix_mem_int(dim, num_sources);
    wave_source_points[0][0] = (int)h/2;
    wave_source_points[1][0] = (int)w/2;
    wave_source_points[0][1] = 0;
    wave_source_points[1][1] = w - 1;


    U = allocate_matrix_mem_double(h, w);
    //init
    for (int i = 0; i < h; i++){
        for (int j = 0; j < w; j++){
            // TODO: ��������, ���� �� �������������/inf �� ���������
            U[i][j] = (is_source(i, j, wave_source_points, num_sources)) ? 0 : INFINITY;
        };
    };
    free_mem_int(wave_source_points, dim);


    double** U_prev = allocate_matrix_mem_double(h, w);
    deepcopy(U, U_prev, h, w);

    while( !domain_finished ){
        iter_for_domain(U, U_prev, image, h, w);
        temp = U_prev;
        U_prev = U;
        U = temp;
        iter_counter++;
    };

    double** scaled = scale_matrix(U, h, w);
    print_matrix_double(U, 10, 10);
    save_distance_matrix(scaled, h-4, w-4, "GWDT.pgm");
    printf("\n Fragment of the final distance image matrix: ");
    print_matrix_double(scaled, 10, 10);

    int status = system("convert GWDT.pgm -colorspace gray GWDT.jpg");
    if (!status) printf("Transformed image is GWDT.jpg. Check it out!\n");


    free_mem_int(image, h);
    free_mem_double(U, h);
    free_mem_double(scaled, h - 4);


    return 0;
    };

